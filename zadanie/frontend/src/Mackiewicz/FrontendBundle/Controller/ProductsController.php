<?php

namespace Mackiewicz\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;
use Mackiewicz\FrontendBundle\Form\ProductsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ProductsController extends Controller
{
    /**
     * @Route("/index", name="index")
     */
    public function indexAction()
    {
            $client = new Client(['base_uri' => $this->getParameter('api_url')]);
            $response = $client->get('products');
            return $this->render(
                '@MackiewiczFrontend/Products/index.html.twig',
                array('data' => json_decode($response->getBody()))
            );
    }

    /**
     * Creates a new order entity.
     *
     * @Route("/product/new", name="new")
     * @Template("@MackiewiczFrontend/Products/new.html.twig")
     */
    public function newAction(Request $request)
    {


        $form = $this->createForm(ProductsType::class, null);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
                $data = json_encode($form->getData(), true);

                $client = new Client(['base_uri' => $this->getParameter('api_url')]);
                $client->post("products", [
                    'headers' => ['Content-Type' => 'application/json'],
                    'body' => $data,
                ]);
                return $this->redirectToRoute('index');
        }

            return array(
                'form' => $form->createView(),
            );
    }

    /**
     * Creates a new order entity.
     *
     * @Route("/product/edit/{id}", name="edit",requirements={"id": "\d+"})
     * @Template("@MackiewiczFrontend/Products/new.html.twig")
     */
    public function editAction(Request $request, $id = 0)
    {

        $client = new Client(['base_uri' => $this->getParameter('api_url')]);
        $response = $client->get("products/$id");
        $form = $this->createForm(ProductsType::class, null);
        $form->setData(json_decode($response->getBody(), true));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $client = new Client(['base_uri' => $this->getParameter('api_url')]);
            $client->put("products/$id", ['body' => json_encode($data)]);
            return $this->redirectToRoute('index');
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a delete row from part entity.
     *
     * @Route("/product/delete/{id}", name="delete",requirements={"id": "\d+"})
     *
     */

    public function deleteAction($id = 0)
    {
        $client = new Client(['base_uri' => $this->getParameter('api_url')]);
        $client->delete("products/$id");

        return $this->redirectToRoute('index');
    }
}
