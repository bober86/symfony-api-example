Symfony Standard Edition
========================

Instalacja projektu
[Installation][1]

Backend ma dostępne końcówki:                      
`GET /products `

`GET POST PUT DELETE /products/{id}`

`GET /products/greaterthan/{count}` 


Przy końcówce `GET /products`  mamy dostępny parametr `category`                        
`category=unavailable`
`category=available`
Zwraca on odpowednio niedostępne i dostępen produkty

Końcówka `GET /products/greaterthan/{count}`   
zwraca nam znajdują się na składzie w ilości większej niż x
np.  `/products/greaterthan/5` 
Zwróci produkty znajdujące się na stanie w ilości większej niż 5

Cały routing możemy zobaczyc w konsoli za pomocą
`php bin/console d:r`

[1]:  https://symfony.com/doc/3.4/setup.html
