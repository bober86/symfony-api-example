<?php

namespace Mackiewicz\ApiBundle\Manager;

use Doctrine\ORM\EntityManager;
use Mackiewicz\ApiBundle\Entity\Products;

class ProductsManager
{
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function new()
    {
        return new Products();
    }

    public function save(Products $product)
    {

        $repo = $this->em;
        $repo->persist($product);
        $repo->flush();

        return $product;
    }

    public function getUnAvailable()
    {

        return $this->em->getRepository('MackiewiczApiBundle:Products')->getUnAvailable();
    }


    public function getAvailable()
    {

        return $this->em->getRepository('MackiewiczApiBundle:Products')->getAvailable();
    }

    public function getGreaterThan($count = 0)
    {

        return $this->getDoctrine()->getRepository('MackiewiczApiBundle:Products')
            ->getGreaterThan($count);
    }

}