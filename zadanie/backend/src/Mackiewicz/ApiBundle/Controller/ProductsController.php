<?php

namespace Mackiewicz\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Mackiewicz\ApiBundle\Form\ProductsType;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\QueryParam;

class ProductsController extends FOSRestController implements ClassResourceInterface
{
    /**
     * @QueryParam(name="category", default="1", description="category to get avalible, unavalible")
     * @Rest\Get("/products")
     */
    public function getAction(ParamFetcher $paramFetcher)
    {

        if ($paramFetcher->get('category')) {
            if ($paramFetcher->get('category') == 'unavailable') {
                return $this->container->get('mackiewicz_api.product.manager')->getUnAvailable();
            } elseif ($paramFetcher->get('category') == 'available') {
                return $this->container->get('mackiewicz_api.product.manager')->getAvailable();
            }
        }

        return $this->getDoctrine()
            ->getRepository('MackiewiczApiBundle:Products')
            ->findBy([], ['id' => 'ASC']);
    }

    /**
     * @Rest\Post("/products")
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(ProductsType::class, null);
        $form->submit($request->request->all());
        if (!$form->isValid()) {
            return $form;
        }
        $this->container->get('mackiewicz_api.product.manager')->save($form->getData());


        return $this->view(array("messages" => 'product added'), 201);
    }



    /**
     * @Rest\Get("/products/{id}")
     */
    public function getByIdAction($id = 0)
    {

        return $this->getDoctrine()->getRepository('MackiewiczApiBundle:Products')
            ->findOneBy(['id' => $id]);
    }

    /**
     * @Rest\Put("/products/{id}")
     */
    public function putAction($id, Request $request)
    {
        $product = $this->getDoctrine()
            ->getRepository('MackiewiczApiBundle:Products')
            ->findOneBy(['id' => $id]);
        if (!$product) {
            return $this->view(array("messages" => 'product not found'), 404);
        }
        $data = json_decode($request->getContent(), true);
        $form = $this->createForm(ProductsType::class, $product);
        $form->submit($data);
        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();

        return $this->view(array("messages" => 'product updated'), 200);
    }

    /**
     * @Rest\Delete("/products/{id}")
     */
    public function deleteByIdAction($id = 0)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('MackiewiczApiBundle:Products')->findOneBy(['id' => $id]);

        if (!empty($product)) {
            $em->remove($product);
            $em->flush();
            $view = $this->view(array(), 204);
        } else {
            $view = $this->view(array("messages" => 'product not found'), 404);
        }

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("/products/greaterthan/{count}")
     */
    public function getGreaterThan($count = 0)
    {
        return $this->container->get('mackiewicz_api.product.manager')->getGreaterThan($count);
    }
}
