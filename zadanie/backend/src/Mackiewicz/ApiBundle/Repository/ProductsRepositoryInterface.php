<?php

namespace Mackiewicz\ApiBundle\Repository;

interface ProductsRepositoryInterface
{

    public function getAvailable();

    public function getUnAvailable();

    public function getGreaterThan(int $count);

}