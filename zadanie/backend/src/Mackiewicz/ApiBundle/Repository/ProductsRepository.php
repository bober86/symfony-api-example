<?php

namespace Mackiewicz\ApiBundle\Repository;

/**
 * ProductsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProductsRepository extends \Doctrine\ORM\EntityRepository implements ProductsRepositoryInterface
{

    public function getAvailable()
    {
        return $this->getData($this->getEntityManager()->createQueryBuilder()->expr()->gt('p.amount', 0));
    }

    public function getUnAvailable()
    {
        return $this->getData($this->getEntityManager()->createQueryBuilder()->expr()->eq('p.amount', 0));
    }

    public function getGreaterThan(int $count)
    {

        return $this->getData($this->getEntityManager()->createQueryBuilder()->expr()->gt('p.amount', $count));
    }

    public function getData($cond)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $q  = $qb->select(array('p'))
            ->from('MackiewiczApiBundle:Products', 'p')
            ->where(
                $cond
            )
            ->orderBy('p.amount', 'DESC')
            ->getQuery();
        return $q->getResult();
    }

}
